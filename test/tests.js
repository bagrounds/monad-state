;(() => {
  'use strict'

  /* imports */
  const { equalDeep: equal } = require('fun-predicate')
  const { get, map: oMap } = require('fun-object')
  const curry = require('fun-curry')
  const { concat, map } = require('fun-array')
  const { sync } = require('fun-test')
  const { add } = require('fun-scalar')

  const { leftId } = (() => {
    /* eslint-disable max-params */

    return oMap(curry, {

      /* Monad Laws */
      leftId: ([eq, f, x0, x1], { of, chain }) =>
        eq(chain(of(x0), f, x1), f(x0, x1))
    })
    /* eslint-enable max-params */
  })()

  const propertyTests = [
    {
      inputs: [[equal, (a, b) => [a + b, a - b], 7, 9]],
      predicate: equal(true),
      contra: s => i => leftId(i, s)
    }
  ]

  const inc = s => [s + 1, s]
  const equalityTests = [
    {
      inputs: [1, inc],
      predicate: equal(2),
      contra: get('run')
    },
    {
      inputs: [add, inc, inc, 0],
      predicate: equal([2, 0]),
      contra: get('liftM2')
    }
  ]

  /* exports */
  module.exports = map(sync, concat(propertyTests, equalityTests))
})()

