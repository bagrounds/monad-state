# [monad-state](https://bagrounds.gitlab.io/monad-state)

The State Monad

[![build-status](https://gitlab.com/bagrounds/monad-state/badges/master/build.svg)](https://gitlab.com/bagrounds/monad-state/commits/master)
[![coverage-report](https://gitlab.com/bagrounds/monad-state/badges/master/coverage.svg)](https://gitlab.com/bagrounds/monad-state/commits/master)
[![license](https://img.shields.io/npm/l/monad-state.svg)](https://www.npmjs.com/package/monad-state)
[![version](https://img.shields.io/npm/v/monad-state.svg)](https://www.npmjs.com/package/monad-state)
[![downloads](https://img.shields.io/npm/dt/monad-state.svg)](https://www.npmjs.com/package/monad-state)
[![downloads-monthly](https://img.shields.io/npm/dm/monad-state.svg)](https://www.npmjs.com/package/monad-state)
[![dependencies](https://david-dm.org/bagrounds/monad-state/status.svg)](https://david-dm.org/bagrounds/monad-state)

## [Test Coverage](https://bagrounds.gitlab.io/monad-state/coverage/lcov-report/index.html)

## [API Docs](https://bagrounds.gitlab.io/monad-state/index.html)

## Dependencies

### Without Tests

![Dependencies](https://bagrounds.gitlab.io/monad-state/img/dependencies.svg)

### With Tests

![Test Dependencies](https://bagrounds.gitlab.io/monad-state/img/dependencies-test.svg)

